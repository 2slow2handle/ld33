package;

import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;

class Player extends FlxSprite
{
    public static inline var GRAVITY:Float = 800;

    public var fsm:FlxFSM<FlxSprite>;
    public static var hitJumper:Bool=false;
    public static var running:Bool=false;
    public static var climbing:Bool=false;
    public static var hitWall:Bool=false;

    public function new(X:Float = 0, Y:Float = 0)
    {
        super(X, Y);
        __reset();

        loadGraphic("assets/images/player.png", true, 16, 16);
        setFacingFlip(FlxObject.LEFT, true, false);
        setFacingFlip(FlxObject.RIGHT, false, false);
        facing = FlxObject.RIGHT;

        animation.add("standing", [0, 1], 3);
        animation.add("walking", [0, 1], 12);
        animation.add("jumping", [2]);
        animation.add("pound", [3]);
        animation.add("landing", [4, 0, 1, 0], 8, false);
        animation.add("stun", [4, 5], 3);

        acceleration.y = GRAVITY;
        maxVelocity.set(100, GRAVITY);

        fsm = new FlxFSM<FlxSprite>(this);
        fsm.transitions
        .add(Idle, Jump, Conditions.jump)
        .add(Jump, Idle, Conditions.grounded)
        .add(Jump, GroundPound, Conditions.groundSlam)
        .add(Idle, KickedUp, Conditions.hitJumper)
        .add(KickedUp, Idle, Conditions.grounded)
        .add(GroundPound, GroundPoundFinish, Conditions.grounded)
        .add(GroundPoundFinish, Idle, Conditions.animationFinished)
        .add(Jump, Stun, Conditions.stunned)
        .add(KickedUp, Stun, Conditions.stunned)
        .add(Idle, Stun, Conditions.stunned)
        .start(Idle);
    }

    private function __reset():Void {
        hitJumper = false;
        hitWall = false;
        running = false;
        climbing = false;
    }

    override public function update(elapsed:Float):Void
    {
        if (FlxG.keys.justPressed.Q) {
            for (mine in Reg.playState.mines) {
                if (Math.abs(this.x - mine.x) < 200 && Math.abs(this.y - mine.y) < 200) {
                    FlxG.log.add("exaploding mine at " + mine.x + ", " + mine.y);
                    mine.explode();
                }
            }
        }

        fsm.update(elapsed);
        super.update(elapsed);
    }

    override public function destroy():Void
    {
        fsm.destroy();
        fsm = null;
        super.destroy();
    }
}

class Conditions
{
    public static function jump(Owner:FlxSprite):Bool
    {
        return !Player.climbing && FlxG.keys.justPressed.UP && Owner.isTouching(FlxObject.DOWN);
    }

    public static function grounded(Owner:FlxSprite):Bool
    {
        return Owner.isTouching(FlxObject.DOWN);
    }

    public static function groundSlam(Owner:FlxSprite)
    {
        return FlxG.keys.justPressed.DOWN && !Owner.isTouching(FlxObject.DOWN);
    }

    public static function animationFinished(Owner:FlxSprite):Bool
    {
        return Owner.animation.finished;
    }

    public static function hitJumper(Owner:FlxSprite):Bool
    {
        return (Owner.isTouching(FlxObject.DOWN) && Player.hitJumper);
    }

    public static function climb(Owner:FlxSprite):Bool
    {
        return Player.climbing && (FlxG.keys.pressed.DOWN || FlxG.keys.pressed.UP);
    }

    public static function notClimb(Owner:FlxSprite):Bool
    {
        return (FlxG.keys.pressed.LEFT || FlxG.keys.pressed.UP);
    }

    public static function stunned(Owner:FlxSprite):Bool
    {
        return Owner.isTouching(FlxObject.DOWN) && Player.hitWall;
    }
}

class Idle extends FlxFSMState<FlxSprite>
{
    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("standing");
        Player.hitJumper = false;
        Player.climbing = false;
        FlxG.log.add('Idle enter');
        owner.velocity.y = Player.GRAVITY;
    }

    override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        if (Player.running) {
            owner.animation.play("walking");
            owner.acceleration.x = 300;
        } else {
            owner.acceleration.x = 0;
            if (FlxG.keys.pressed.LEFT || FlxG.keys.pressed.RIGHT)
            {
                owner.facing = FlxG.keys.pressed.LEFT ? FlxObject.LEFT : FlxObject.RIGHT;
                owner.animation.play("walking");
                owner.acceleration.x = FlxG.keys.pressed.LEFT ? -300 : 300;
            }
            else
            {
                owner.animation.play("standing");
                owner.velocity.x *= 0.9;
            }
        }
    }
}

class Jump extends FlxFSMState<FlxSprite>
{
    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("jumping");
        owner.velocity.y = -250;
        Reg.playState.jumpSound.play();
        FlxG.log.add(Reg.playState.player.x);
    }

    override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        if (Player.running) {
            owner.acceleration.x = 300;
        } else {
            owner.acceleration.x = 0;
            if (FlxG.keys.pressed.LEFT || FlxG.keys.pressed.RIGHT)
            {
                owner.acceleration.x = FlxG.keys.pressed.LEFT ? -300 : 300;
            }
        }

        if (owner.velocity.y > 0) {
            owner.animation.play("pound");
        }

    }
}

class SuperJump extends Jump
{
    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("jumping");
        owner.velocity.y = -300;
    }
}

class KickedUp extends Jump
{
    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("jumping");
        Reg.playState.kickUpSound.play();
        owner.velocity.y = -400;
    }
}

class Stun extends FlxFSMState<FlxSprite>
{
    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("stun");
        Player.running = false;
        owner.acceleration.x = 0;
        owner.velocity.x = -owner.velocity.x * 5;
    }

    override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.velocity.x *= 0.5;
    }
}

class Climb extends FlxFSMState<FlxSprite>
{
    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("standing");
        owner.velocity.y = 0;
        FlxG.log.add('Climb enter');
    }

    override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
//        owner.acceleration.y = 0;
//
        if (FlxG.keys.pressed.UP || FlxG.keys.pressed.DOWN)
        {
            owner.velocity.y = FlxG.keys.pressed.UP ? -50 : 50;
        } else {
            owner.velocity.y = 0;
        }

        if (FlxG.keys.pressed.LEFT || FlxG.keys.pressed.RIGHT)
        {
            owner.velocity.x = FlxG.keys.pressed.LEFT ? -50 : 50;
        } else {
            owner.velocity.x = 0;
        }
//
        Player.climbing = false;
    }
}

class GroundPound extends FlxFSMState<FlxSprite>
{
    private var _ticks:Float;

    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("pound");
        owner.velocity.x = 0;
        owner.acceleration.x = 0;
        _ticks = 0;
    }

    override public function update(elapsed:Float, owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        _ticks++;
        if (_ticks < 15)
        {
            owner.velocity.y = 0;
        }
        else
        {
            owner.velocity.y = Player.GRAVITY;
        }
    }
}

class GroundPoundFinish extends FlxFSMState<FlxSprite>
{
    override public function enter(owner:FlxSprite, fsm:FlxFSM<FlxSprite>):Void
    {
        owner.animation.play("landing");
        FlxG.camera.shake(0.025, 0.25);
        owner.velocity.x = 0;
        owner.acceleration.x = 0;
    }
}


class Girl extends FlxSprite
{
    public function new(X:Float = 0, Y:Float = 0)
    {
        super(X, Y);

        loadGraphic("assets/images/girlfriend.png", true, 16, 16);
        animation.add("walking", [0, 1], 12);
        animation.play("walking");
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);
    }

    override public function destroy():Void
    {
        super.destroy();
    }
}