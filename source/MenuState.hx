package;

import flixel.system.debug.Tracker.TrackerProfile;
import Std;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.debug.FlxDebugger;
//import flixel.system.debug.FlxDebuggerLayout;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.system.FlxSound;
import pgr.dconsole.DC;

/**
 * A FlxState which can be used for the game's menu.
 */
class MenuState extends FlxState
{
    private var background:FlxSprite;
    private var title:FlxText;
    private var BtnRun:FlxButton;
    private var BtnAbout:FlxButton;
    private var buzz:Bool=false;
    private var buzzTicks:Int=0;
    private var buzzSound:FlxSound;

	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
        FlxG.debugger.visible = false;
        FlxG.debugger.drawDebug = false;
        FlxG.debugger.setLayout(FlxDebuggerLayout.RIGHT);

        background = new FlxSprite();
        background.loadGraphic("assets/images/menu-bg-small.jpg", true, 320, 240);
        //background.animation.add("cycle", [0, 1]);
        background.x = 0;
        background.frame = background.frames.frames[0];// framesData.frames[0];
        add(background);
        //background.animation.play("cycle");

        title = new FlxText(0, (FlxG.height/3) * .5, FlxG.width, "Harmless");
		title.setFormat("assets/fonts/PRINTERROR.TTF", 42, 0xFF69391A, "center");
        title.angle = 2;
        add(title);

        BtnRun = new FlxButton(0, (FlxG.height/3) * 1.8, "START", startGame);
        BtnRun.label.angle = 2;
        BtnRun.label.setFormat("assets/fonts/SHORTCUT.TTF", 24, 0xFF69391A, "center");
        BtnRun.loadGraphic("assets/images/button.png", false, 104, 29);
        BtnRun.x = (FlxG.width - BtnRun.width) * .5;
        add(BtnRun);

        /*
        BtnAbout = new FlxButton(0, (FlxG.height/3) * 2.1, "ABOUT", showCredits);
        BtnAbout.label.angle = 2;
        BtnAbout.label.setFormat("assets/fonts/SHORTCUT.TTF", 20, 0xFF69391A, "center");
        BtnAbout.loadGraphic("assets/images/button.png", false, 104, 29);
        BtnAbout.x = (FlxG.width - BtnAbout.width) * .5;
        add(BtnAbout);
        */

        #if flash
        buzzSound = FlxG.sound.load("assets/sounds/static discharge.mp3", 0.2);
        FlxG.sound.playMusic("assets/music/ambient.mp3", 0.7, true);
        #else
        buzzSound = FlxG.sound.load("assets/sounds/static discharge.ogg", 0.2);
        FlxG.sound.playMusic("assets/music/ambient.ogg", 0.7, true);
        #end

		super.create();
	}

    private function startGame():Void
    {
        DC.log("startGame");
        FlxG.sound.music.stop();
        Reg.playState = new PlayState();
        FlxG.switchState(Reg.playState);
    }

    private function showCredits():Void
    {
        DC.log("showCredits");
        //FlxG.switchState(new PlayState());
    }

    /**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
        background.destroy();
        title.destroy();
        BtnRun.destroy();

        background = null;
        title = null;

		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update(elapsed:Float):Void
	{
        if (!buzz && Std.random(300) == 0) {
            FlxG.log.add("buzz");
            doBuzz();
        }

        // TODO: FlxTimer
        if (buzz) {
            buzzTicks++;
            if (buzzTicks > (20 + Std.random(50))) {
                FlxG.log.add("unbuzz");
                doUnbuzz();
            }
        }

		super.update(elapsed);
	}

    private function doBuzz():Void
    {
        buzzSound.play();
        buzz = true;
        background.frame = background.frames.frames[1];
        title.color = 0xFF0F1D1D;
        BtnRun.label.color = 0xFF000D0D;
        //BtnAbout.label.color = 0xFF000D0D;
    }

    private function doUnbuzz():Void
    {
        buzzSound.stop();
        buzzTicks = 0;
        buzz = false;
        background.frame = background.frames.frames[0];
        title.color = 0xFF69391A;
        BtnRun.label.color = 0xFF69391A;
        //BtnAbout.label.color = 0xFF69391A;
    }
}