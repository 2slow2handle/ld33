package ;

import flixel.system.FlxSound;
import flixel.FlxG;
import flixel.FlxCamera;
import flixel.addons.editors.tiled.TiledPropertySet;
import flixel.FlxObject;
import flixel.util.FlxColor;
import flash.display.Graphics;

class Trigger extends FlxObject
{
    public var data:TiledPropertySet;
    public var oneShot:Bool=false;
    public var fired:Bool=false;

    public function new(Data:TiledPropertySet, X:Float = 0, Y:Float = 0, Width:Float = 0, Height:Float = 0)
    {
        data = Data;
        if (data.contains('one-shot')) {
            oneShot = data.get('one-shot') == 'true' ? true : false;
        }
        super(X, Y, Width, Height);
    }

    public function trigger():Void
    {
        if (oneShot && fired) {
            return;
        }

        switch (data.get('type'))
        {
            case "shake":
                FlxG.cameras.shake(0.02, 0.2);
                Reg.playState.hitSound.play();
                FlxG.log.add('shake trigger');

            case "set_checkpoint":
                var value = Std.parseInt(data.get('value'));
                TiledLevel.lastCheckpoint = value;
                FlxG.log.add('set checkpoint ' + value);

            case "banner":
                var text = data.get('value');
                Reg.playState.banner.text = text;
                FlxG.log.add('show banner '+ text);

            case "play_music":
                var filename = data.get('value');
                var loop = data.get('loop') == "true" ? true : false;
                FlxG.log.add('music trigger' + filename);
                FlxG.sound.music.stop();
                #if flash
                FlxG.sound.playMusic("assets/music/" + filename + ".mp3", 1, loop);
                #else
                FlxG.sound.playMusic("assets/music/" + filename + ".ogg", 1, loop);
                #end

            case "the_end":
                FlxG.camera.fade(FlxColor.WHITE, 5, false, function() {
                    FlxG.sound.music.stop();
                    Reg.playState = null;
                    FlxG.switchState(new MenuState());
                });

            case "run":
                FlxG.log.add('run trigger');
                if (data.get('value') == 'true') {
                    Player.running = true;
                    Reg.playState.player.maxVelocity.x = 300;
                } else {
                    Player.running = false;
                    Reg.playState.player.maxVelocity.x = 100;
                }

            case "kill":
                Player.running = false;
                Reg.playState.player_die();

            case "wall":
                Player.hitWall = true;
                Reg.playState.deathCamera();
                Reg.playState.hitSound.play();
                FlxG.log.add('hit wall trigger');
                FlxG.camera.flash(FlxColor.WHITE, .15);
                FlxG.cameras.shake(0.02, 0.4);
                FlxG.camera.fade(FlxColor.WHITE, 4, false, function() {
                    FlxG.sound.music.stop();
                    Reg.playState = null;
                    Reg.playState = new PlayState();
                    FlxG.switchState(Reg.playState);
                });

            case "jump":
                Player.hitJumper = true;
                FlxG.log.add('jump trigger');

            case "explode_mines":
                var radius = Std.parseInt(data.get('radius'));
                var hack = data.get('hack') == 'true' ? true : false;
                var blip = data.get('blip') == 'true' ? true : false;
                FlxG.log.add('exploding mines in radius ' + radius);

                if (blip) {
                    #if flash
                    FlxG.sound.play('assets/sounds/blip.mp3', 1);
                    #else
                    FlxG.sound.play('assets/sounds/blip.ogg', 1);
                    #end
                }

                if (hack) {
                    var callback = function():Void {
                        for (mine in Reg.playState.mines) {
                            if (Math.abs(this.x - mine.x) < radius && Math.abs(this.y - mine.y) < radius) {
                                FlxG.log.add("exaploding mine at " + mine.x + ", " + mine.y);
                                mine.explode();
                            }
                        }
                    };
                    #if flash
                    FlxG.sound.play('assets/sounds/alarm_hack.mp3', 1, false, true, callback);
                    #else
                    FlxG.sound.play('assets/sounds/alarm_hack.ogg', 1, false, true, callback);
                    #end
                } else {
                    for (mine in Reg.playState.mines) {
                        if (Math.abs(this.x - mine.x) < radius && Math.abs(this.y - mine.y) < radius) {
                            FlxG.log.add("exaploding mine at " + mine.x + ", " + mine.y);
                            mine.explode();
                        }
                    }
                }

//            case "ladder":
//                FlxG.log.add('ladder trigger');
//                Player.climbing = true;
        }

        fired=true;
    }
}



