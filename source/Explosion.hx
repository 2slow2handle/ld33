package;

import flixel.system.FlxSound;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;

class Explosion extends FlxSprite
{
    private var _delay:Float = 0;
    private var _target:FlxSprite;
    private var _pos:FlxPoint;
    private var sound:FlxSound;

    public function new()
    {
        super();
        loadGraphic("assets/images/expl_big.png", true, 64, 64);
        animation.add("explode", [0, 2, 4, 6, 8, 10, 12, 14], 30, false);

        #if flash
        sound = FlxG.sound.load("assets/sounds/explosion.mp3", 0.2);
        #else
        sound = FlxG.sound.load("assets/sounds/explosion.ogg", 0.2);
        #end

        _pos = FlxPoint.get();
    }

    public function explode(Target:FlxSprite, Delay:Float = 0):Void
    {
        _delay = Delay;

        _target = Target;
        _pos.x = FlxG.random.float(-64, _target.width+20);
        _pos.y = FlxG.random.float( -64, _target.height-20);
        reset(_target.x + _pos.x, _target.y + _pos.y);

        visible = false;
    }

    override public function draw():Void
    {
        x = _target.x + _pos.x;
        y = _target.y + _pos.y;
        super.draw();
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);
        if (alive && !visible)
        {
            if (_delay <= 0)
            {
                visible = true;
                sound.play();
                animation.play("explode");
            }
            else
                _delay -= FlxG.elapsed * 6;
        }
        else if (alive && visible)
        {
            if (animation.finished)
            {
                animation.pause();
                kill();
            }
        }
    }
}

enum ExplosionType
{
    BIG;
    BIG_HOLLOW;
    SMALL;
    SMALL_HOLLOW;
    BIG_GND;
    BIG_GND_HOLLOW;
    SMALL_GND;
    SMALL_GND_HOLLOW;
}