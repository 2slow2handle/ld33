package;

import flixel.addons.util.FlxFSM;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;

class Mine extends FlxSprite
{
    public static var armed:Bool=true;
    public static inline var w:Int=16;
    public static inline var h:Int=8;

    public function new(X:Float = 0, Y:Float = 0)
    {
        super(X, Y);

        loadGraphic("assets/images/mine.png", true, w, h);

        animation.add("armed", [0, 1, 2, 3], 3);
        animation.add("disarming", [4, 5, 6, 5, 4, 5 ,6], 12);
        animation.add("disarmed", [7]);

        /*
        fsm = new FlxFSM<FlxSprite>(this);
        fsm.transitions
        .add(Idle, Jump, Conditions.jump)
        .add(Jump, Idle, Conditions.grounded)
        .add(Jump, GroundPound, Conditions.groundSlam)
        .add(Idle, KickedUp, Conditions.hitJumper)
        .add(KickedUp, Idle, Conditions.grounded)
        .add(Idle, Climb, Conditions.climb)
        .add(Climb, Idle, Conditions.notClimb)
        .add(GroundPound, GroundPoundFinish, Conditions.grounded)
        .add(GroundPoundFinish, Idle, Conditions.animationFinished)
        .start(Idle);
        */
        animation.play("armed");
    }

    public function explode():Void
    {
        if (alive)
        {
            kill();
            Reg.playState.addExplosions(this);
            FlxG.camera.flash(FlxColor.WHITE, .1);
            FlxG.cameras.shake(0.01, 0.4);
        }
    }

    override public function update(elapsed:Float):Void
    {
        //fsm.update(elapsed);
        super.update(elapsed);
    }

    override public function destroy():Void
    {
        //fsm.destroy();
        //fsm = null;
        super.destroy();
    }
}