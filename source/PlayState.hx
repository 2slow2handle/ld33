package;

import Player.Girl;
import Trigger;
import flixel.FlxBasic;
import flixel.system.debug.Tracker.TrackerProfile;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxObject;
import flixel.util.FlxTimer;
import flixel.FlxCamera;
//import flixel.util.FlxPoint;
import flash.Lib;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
//import flixel.util.FlxMath;
import flixel.system.FlxSound;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledLayer.TiledLayerType;
import flixel.math.FlxPoint;
import flixel.util.FlxTimer;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import flixel.effects.postprocess.PostProcess;
import flixel.util.FlxColor;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
    public var level:TiledLevel;
    public var player:Player;
    public var ticks:Int=0;
    public var hitSound:FlxSound;
    public var jumpSound:FlxSound;
    public var kickUpSound:FlxSound;
    public var triggers:FlxTypedGroup<Trigger>;
    public var mines:FlxTypedGroup<Mine>;
    public var explosions:FlxTypedGroup<Explosion>;
    public var cameraOffset:FlxPoint=FlxPoint.get(0, 250);
    public var _cameraTween:FlxTween;
    public var banner:FlxText;
    public var girlfriend:Girl;


	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
        FlxG.cameras.bgColor = 0x5A87CB;
        //FlxG.resizeGame(320, 240);
        /*FlxG.stage.width = 320;
        FlxG.stage.height = 240;
        FlxG.camera.zoom = 2;
        */
/*
        var stageWidth = Lib.current.stage.stageWidth;
        var stageHeight = Lib.current.stage.stageHeight;
        //FlxG.resetCameras(new FlxCamera(0, 0, stageWidth, stageHeight));
        FlxG.resizeGame(stageWidth, stageHeight);
        FlxG.camera.setBounds(0, 0,  100,  100);
        */
        //lxG.camera.zoom = 4;

        //FlxG.camera.follow(player, FlxCamera.STYLE_TOPDOWN, 1);

        #if flash
        hitSound = FlxG.sound.load("assets/sounds/hit.mp3", 0.3);
        jumpSound = FlxG.sound.load("assets/sounds/jump.mp3", 0.3);
        kickUpSound = FlxG.sound.load("assets/sounds/kickup.mp3", 0.3);
        #else
        hitSound = FlxG.sound.load("assets/sounds/hit.ogg", 0.3);
        jumpSound = FlxG.sound.load("assets/sounds/jump.ogg", 0.3);
        kickUpSound = FlxG.sound.load("assets/sounds/kickup.ogg", 0.3);
        #end

		level = new TiledLevel("assets/data/level1.tmx");
        triggers = new FlxTypedGroup();
        add(triggers);

        add(level.backgroundTiles);
        add(level.foregroundTiles);

        mines = new FlxTypedGroup();
        add(mines);

        level.loadObjects(this);

        explosions = new FlxTypedGroup();
        add(explosions);

        //FlxG.camera.setBounds(50, 50,  320,  240);
        //player.maxVelocity = FlxPoint.get(20000, 20000);

        new FlxTimer().start(3.0, tweakCamera, 0);

        new TrackerProfile(FlxSprite, ["frameWidth", "frameHeight", "alpha", "origin", "offset", "scale"], [FlxObject]);
        FlxG.debugger.track(player);
        #if cpp
        // FlxG.addPostProcess(new PostProcess("assets/data/scanline.frag"));
        #end

        #if flash
        FlxG.sound.playMusic("assets/music/alarm.mp3", 0.7, true);
        #else
        FlxG.sound.playMusic("assets/music/alarm.ogg", 0.7, true);
        #end

        banner = new FlxText(0, 450, FlxG.width-40, "");
        banner.setFormat(16, 0xFFFFFFFF, "center");
        add(banner);

        girlfriend = new Girl(37505, 592);
        add(girlfriend);

        super.create();
	}

	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update(elapsed:Float):Void
	{
        super.update(elapsed);
        //FlxG.collide(level, player);
        ticks++;
        if (ticks % 3 == 0) {
            FlxG.overlap(triggers, player, onTriggerHit);
            FlxG.overlap(mines, player, onMineHit);
        }

        level.collideWithLevel(player);

        banner.x = player.x + 10;

        if (player.x > girlfriend.x - 20) {
            girlfriend.x = player.x + 20;
            girlfriend.y = player.y;
        }


//        player.acceleration.x = 0;
//        if (FlxG.keys.pressed.LEFT)
//        {
//            player.acceleration.x = -player.maxVelocity.x * 4;
//        }
//        if (FlxG.keys.pressed.RIGHT)
//        {
//            player.acceleration.x = player.maxVelocity.x * 4;
//        }
//        if (FlxG.keys.pressed.SPACE && player.isTouching(FlxObject.FLOOR))
//        {
//            player.velocity.y = -player.maxVelocity.y / 1.5;
//        }
//
//		super.update(elapsed);
//
//        FlxG.overlap(hitboxes, player, hitWall);
//        FlxG.overlap(jumpers, player, hitJumper);
//
//        level.collideWithLevel(player);
	}

    public function addExplosions(Target:FlxSprite):Void
    {
        var e:Explosion;
        for (i in 0...3)
        {
            e = explosions.recycle();
            if (e == null)
            {
                e = new Explosion();
            }
            e.explode(Target, i);
            explosions.add(e);
        }
    }

//    public function onWallHit(Hitbox:FlxObject, Player:FlxObject)
//    {
//        FlxG.log.add("HIT!");
//        hitSound.play();
//        FlxG.cameras.shake(0.005, 0.4);
//        player.acceleration.x = - player.maxVelocity.x * 10;
//        player.velocity.x = - player.maxVelocity.x * 4;
//    }

    public function onTriggerHit(Trigger:Trigger, Player:FlxObject)
    {
        Trigger.trigger();
    }

    public function player_die() {
        player.kill();
        FlxG.camera.fade(FlxColor.BLACK, 3, false, function() {
            FlxG.sound.music.stop();
            // FlxG.resetState(); // не работает (из-за ссылки в Reg?)

            Reg.playState = null;
            Reg.playState = new PlayState();
            FlxG.switchState(Reg.playState);
        });
    }

    public function onMineHit(Mine:Mine, Player:FlxObject)
    {
        Mine.explode();
        player_die();
    }

    private function tweakCamera(Timer:FlxTimer):Void {
        _cameraTween = FlxTween.tween(cameraOffset, { x:-player.velocity.x }, 2.95, { ease: FlxEase.cubeInOut, onUpdate: moveCamera});
    }

    private function moveCamera(Tween:FlxTween):Void {
        FlxG.camera.follow(player, FlxCameraFollowStyle.LOCKON, cameraOffset, 0.5);
    }

    public function deathCamera():Void {
        _cameraTween.cancel();
        FlxTween.tween(cameraOffset, { x:0 }, 0.5, { ease: FlxEase.cubeInOut, onUpdate: moveCamera});
    }
}